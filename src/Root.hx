import starling.display.Sprite;
import starling.display.Sprite3D;
import starling.utils.AssetManager;
import starling.display.Image;
import starling.core.Starling;
import starling.animation.Transitions;
import starling.display.Quad;
import starling.events.KeyboardEvent;
import starling.events.EnterFrameEvent;
import starling.text.TextField;
import starling.animation.Tween;
import starling.animation.Juggler;
import starling.textures.TextureSmoothing;
import starling.textures.Texture;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.media.Sound;
import flash.media.SoundChannel;
import Math;


class Root extends Sprite {

    public static var assets:AssetManager;

    public function new() {
        super();
        scaleX = 8;
        scaleY = 8;
    }
    public function initialize(startup:Startup) {
        assets = new AssetManager();
        // enqueue here
        assets.textureRepeat = true;
        assets.enqueue("assets/desert.png");
        assets.textureRepeat = false;
        
        assets.enqueue("assets/deathBanner1.png");
        assets.enqueue("assets/deathBanner2.png");
        assets.enqueue("assets/deathBanner3.png");
        assets.enqueue("assets/deathBanner4.png");
        assets.enqueue("assets/avatar_walk0.png");
	    assets.enqueue("assets/avatar_walk1.png");
	    assets.enqueue("assets/avatar_jump.png");

        assets.enqueue("assets/cloud1.png");
        assets.enqueue("assets/cloud2.png");
        assets.enqueue("assets/cloud3.png");
        assets.enqueue("assets/cloud4.png");

        assets.enqueue("assets/cactus1.png");
        assets.enqueue("assets/cactus2.png");
        assets.enqueue("assets/cactus3.png");
        assets.enqueue("assets/cactus4.png");
        assets.enqueue("assets/fatigue_bar.png");
        assets.enqueue("assets/swoosh.mp3");
        assets.enqueue("assets/otomata1.mp3");
        assets.loadQueue(function onProgress(ratio:Float) {
            if(ratio == 1) {
                Starling.juggler.tween(startup.loadingBitmap,
                    1.0,
                    {
                        transition: Transitions.EASE_OUT,
                        delay: 1.0,
                        alpha: 0,
                        onComplete: function()
                        {
                            startup.removeChild(startup.loadingBitmap);
                        }
                    });
                start();
            }
        });
        Transitions.register("jump", function(ratio:Float):Float {
                return jump_transition(ratio);
        });

        Transitions.register("jump_up", function(ratio:Float):Float {
                return jump_transition(ratio/2);
        });

        Transitions.register("jump_down", function(ratio:Float):Float {
                return 1-jump_transition(1/2+ratio/2);
        });
    }


	var scoreText:TextField;


    private static function jump_transition(ratio:Float):Float{
        var v:Float = 4.0;
        var a:Float = -2*v;
        return v*ratio + 1/2*a*ratio*ratio;
    }

    var score = 0;

    var ground = 40;

    var clock = 0.0;
    var last_double = 0.0;
     
    //var guy:Quad;
    //TODO change guy to /assets/avatar.png from Quad
    var guy:Image;
    var guy_juggler:Juggler;

    var cacti:List<Image>;
    var banners:List<Image>;
    var jump_tween:Tween;
    
    var clouds:List<Image>;

    var sky:Quad;
    var cloud_layer:Sprite;
    var desert:ScrollImage;
    var fatigue_sprite:Sprite;

    var music_channel : SoundChannel;

    public function start(){
        if(music_channel == null) {
            music_channel = assets.getSound("otomata1").play(0,999);
        }
        cacti = new List<Image>(); 
        last_double = -10;   
	    score = 0;

        cacti = new List<Image>();
        last_double = -10;

        var dark_sky = 0x77B5FE;
        var light_sky = 0x8CBED6;
        sky = new Quad(120,40,dark_sky);
        sky.setVertexColor(2,light_sky);
        sky.setVertexColor(3,light_sky);
        addChild(sky);  

        cloud_layer = new Sprite();
        this.addChild(cloud_layer);

        for(i in 0...8) 
        {
            clouds_go(true);
        }


        var ground_3d:Sprite3D = new Sprite3D();
        ground_3d.x = -4;
        ground_3d.y = ground;
        ground_3d.rotationX = -Math.PI/2 + Math.PI/1024 ;
        ground_3d.z = 0;
        addChild(ground_3d);

        desert = new ScrollImage(assets.getTexture("desert"));
        desert.x = 0;
        desert.y = 0;
        desert.scaleY = 13.5;
        desert.smoothing = TextureSmoothing.NONE;
        roll_desert();
        ground_3d.addChild(desert);


        //TODO change guy to /assets/avatar.png from Quad
        //guy = new Quad(4,8,0x000000);
       	guy = new Image(assets.getTexture("avatar_walk1"));
        guy.smoothing = TextureSmoothing.NONE;
        guy.x = 30-2;
        guy.y = ground - 8;
        this.addChild(guy);
        guy_juggler = new Juggler();
        animate_guy();
	
	

        scoreText = new TextField(50, 50, "0", "System", 6, 0x000993);
        scoreText.x = 72;
        scoreText.y = -2;
        scoreText.hAlign = starling.utils.HAlign.RIGHT;
        scoreText.vAlign = starling.utils.VAlign.TOP;
        this.addChild(scoreText);
        scoreText.redraw();
        cast(scoreText.getChildAt(0), Image).smoothing = TextureSmoothing.NONE;

        var fatigue_bar = new Image(assets.getTexture("fatigue_bar"));
        fatigue_bar.smoothing = TextureSmoothing.NONE;

        fatigue_sprite = new Sprite();
        fatigue_sprite.x = 1;
        fatigue_sprite.y = 41;
        fatigue_sprite.scaleX = 0.5;
        fatigue_sprite.scaleY = 0.5;

        fatigue_sprite.addChild(fatigue_bar);
        fatigue_sprite.clipRect=new Rectangle(0,0,7,0);
        //fatigue_sprite.clipRect.height = 38;
        addChild(fatigue_sprite);

        addEventListener(KeyboardEvent.KEY_DOWN, jump_listener);
        addEventListener(flash.events.Event.ENTER_FRAME, on_frame);

    }

    private function animate_guy(){
        var frames = assets.getTextures("avatar_walk");
        var delay = 0.2;
        guy.texture = frames[frames.length-1];
        guy_juggler.purge();
        for(i in 0...frames.length){
            guy_juggler.delayCall(
                function() { 
                    guy.texture = frames[i];
                },
                delay*(i+1));
        }
        guy_juggler.delayCall(function(){animate_guy();},frames.length*delay);
    }
   
    private function clouds_go(at_start:Bool){
        clouds = new List<Image>();
        var cloud_textures = assets.getTextures("cloud");
        var cloud = new Image(cloud_textures[Std.random(cloud_textures.length)]);
        cloud.smoothing = TextureSmoothing.NONE;
        cloud.width = 8;
        cloud.height = 4;
        cloud.scaleX = 2;
        cloud.scaleY = 2;
        cloud.y = Std.random(36)-1;
        cloud.x = 120;
        clouds.add(cloud);
        cloud_layer.addChild(cloud);

        var travel_time = Std.random(40)+40;

        var cloud_tween = new Tween(cloud, travel_time);
        cloud_tween.delay = 0.0;
        cloud_tween.animate("x", -cloud.width);
        cloud_tween.onComplete = function() {
                clouds.remove(cloud);
                removeChild(cloud);
                clouds_go(false);
            }
        Starling.juggler.add(cloud_tween);
        if(at_start) {
            cloud_tween.advanceTime(travel_time*Math.random());
        }
    }



    private function roll_desert(){
        desert.scrollX = 0;
        var speed = (3.0/123)*desert.width;
        Starling.juggler.tween(desert, speed, {
                scrollX: 1.0,
                onComplete: roll_desert
                });
    }

    private function jump_listener(e:KeyboardEvent){
        removeEventListener(KeyboardEvent.KEY_DOWN, jump_listener);
        addEventListener(KeyboardEvent.KEY_DOWN, double_jump_listener);
        var swoosh = assets.playSound("swoosh",150);
        guy_juggler.purge();
        guy.texture = assets.getTexture("avatar_jump");
        jump_tween = new Tween(guy, 1.2, "jump");
        jump_tween.animate("y", ground-8-20);
        jump_tween.onComplete = function() {
                    addEventListener(KeyboardEvent.KEY_DOWN, jump_listener);
                    removeEventListener(KeyboardEvent.KEY_DOWN, double_jump_listener);
                    animate_guy();
                };
        Starling.juggler.add(jump_tween);
    }

    private function double_jump_listener(e:KeyboardEvent){
        if(clock < last_double+10) return;
        last_double = clock;
        removeEventListener(KeyboardEvent.KEY_DOWN, double_jump_listener);
        Starling.juggler.remove(jump_tween);
        fatigue_sprite.clipRect.height=38.0;
        Starling.juggler.tween(fatigue_sprite.clipRect,
            10.0,
            {
                height:0,
            });
        Starling.juggler.tween(guy,
            0.6,
            {
                transition: "jump_up",
                y: guy.y-20,
            });
        Starling.juggler.tween(guy,
            0.6,
            {
                transition: "jump_down",
                delay:0.6,
                y: ground-8,
                onComplete:function()
                {
                    addEventListener(KeyboardEvent.KEY_DOWN, jump_listener);
                    removeEventListener(KeyboardEvent.KEY_DOWN, double_jump_listener);
                    animate_guy();
                }
            });
    }
    


    private function on_frame(e:EnterFrameEvent){
        var prev_clock = clock;
        clock += e.passedTime;

        guy_juggler.advanceTime(e.passedTime);

        // Workaround for property setter not getting called
        desert.scrollX = desert.scrollX;

        var interval = 0.1;

        if(Math.floor(prev_clock / interval) != Math.floor(clock / interval)){
            var chances:Array<Float> = [1.0, 0.25, 0.045, 0.01, 0.0];
            if(Math.random() < chances[cacti.length]){
                cactus_go();
            }
        }
        check_collisions();
    }

    private function check_collisions(){
        for(cactus in cacti){
            if(guy.getBounds(this).intersects(cactus.getBounds(this)))
                die();
        }
    }


    private function die(){
        var banner_textures = assets.getTextures("deathBanner");
        var death_banner = new Image(banner_textures[Std.random(banner_textures.length)]);
        death_banner.smoothing = TextureSmoothing.NONE;
        death_banner.width = 40;
        death_banner.height = 20;
        death_banner.x = 60-20;
        death_banner.y = 10;
        addChild(death_banner);
        removeEventListeners();
        Starling.juggler.purge();
	    addEventListener(KeyboardEvent.KEY_DOWN, function(e:KeyboardEvent){
								removeChildren();
								removeEventListeners();
								start();
								});
	
    }




    private function cactus_go(){
        var cactus_textures = assets.getTextures("cactus");
        var cactus = new Image(cactus_textures[Std.random(cactus_textures.length)]);
        cactus.color = 0xCCCCCC;
        cactus.smoothing = TextureSmoothing.NONE;
        cactus.width = 4;
        cactus.height = 9;
        cactus.y = ground - cactus.height;
        cactus.x = 120;
        cacti.add(cactus);
        this.addChild(cactus); 
        Starling.juggler.tween(cactus,
                3.0,
                {
                    transition: Transitions.LINEAR,
                    delay: 1.0,
                    x: -cactus.width,
                    onComplete: function() {
			score = score + 1;
			scoreText.text = "" + score;
                        cacti.remove(cactus);
                        removeChild(cactus);
                    }
                });
    }
}

class ScrollImage extends Image {
    public var scrollX(default, set):Float = 0;
    public var scrollY(default, set):Float = 0;

    /*public function new(){
        super();
        scrollX = 0;
        scrollY = 0;
    }*/

    public function set_scrollX(value){
        scrollX = value;
        resolve_scroll();
        return scrollX;
        
    }

    public function set_scrollY(value:Float){
        scrollY = value;
        resolve_scroll();
        return scrollY;
        
    }

    public function resolve_scroll(){
        setTexCoords(0, new Point(scrollX, scrollY));
        setTexCoords(1, new Point(scrollX+1, scrollY));
        setTexCoords(3, new Point(scrollX+1, scrollY+1));
        setTexCoords(2, new Point(scrollX, scrollY+1));
    }
}
